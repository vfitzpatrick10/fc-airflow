from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)
    
args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
    'retries': 2,
    'retry_delay': timedelta(minutes=3)
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='update-party-view-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval='0 4 * * *',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=3,
    max_active_runs=1,
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    party_update = KubernetesPodOperator(
        resources=pod.small_resources(),
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Party-View-Update',
        task_id =f'party_view_update',
        image=f"{cr_registry}/forecasa-dbt",
        arguments=['dbt', 'run', '--models', 'vw_party_tbl'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=2),
        get_logs=False
    )

    start >> party_update