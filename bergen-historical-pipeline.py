from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

def get_date_to():
    base_parameters = Variable.get('bergen-scraper', deserialize_json=True)
    date_to  = base_parameters.get('date_to','date_to')

    if not date_to:
        date_to = str(datetime.now().date())
    
    return date_to

def get_historic_params(county):
    params = Variable.get('historical', deserialize_json=True)
    return params.get(county)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'historical'

slack_conn_id = 'airflow-prd-slack'
date_to = get_date_to()

resources = {
    "request_memory": "12Gi",
    "limit_memory": "12Gi",
    "request_cpu": "3500m",
    "limit_cpu": "3500m"
}
big_resources = {
    "request_memory": "27Gi",
    "limit_memory": "27Gi",
    "request_cpu": "6000m",
    "limit_cpu": "6000m"
}

with DAG(
    dag_id='bergen-historical-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=3,
    max_active_runs=1,
) as dag:

    start = slack.message('start', slack_conn_id, dag)
    params = get_historic_params('bergen')

    bergen_DEED_data_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-DEED-data-Scraper',
        task_id =f'bergen_DEED_data_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=DEED', f'--scrape_type=data','--historic_run', f'--date_from={params.get("DEED").get("date_from")}', f'--date_to={params.get("DEED").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    bergen_NS_data_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-NS-data-Scraper',
        task_id =f'bergen_NS_data_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=NS', f'--scrape_type=data','--historic_run', f'--date_from={params.get("NS").get("date_from")}', f'--date_to={params.get("NS").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    bergen_MTG_data_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-MTG-data-Scraper',
        task_id =f'bergen_MTG_data_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=MTG', f'--scrape_type=data','--historic_run', f'--date_from={params.get("MTG").get("date_from")}', f'--date_to={params.get("MTG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    bergen_SAT_data_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-SAT-data-Scraper',
        task_id =f'bergen_SAT_data_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=SAT', f'--scrape_type=data','--historic_run', f'--date_from={params.get("SAT").get("date_from")}', f'--date_to={params.get("SAT").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    bergen_ASM_data_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-ASM-data-Scraper',
        task_id =f'bergen_ASM_data_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=ASM', f'--scrape_type=data','--historic_run', f'--date_from={params.get("ASM").get("date_from")}', f'--date_to={params.get("ASM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    bergen_DEED_pdf_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-DEED-pdf-Scraper',
        task_id =f'bergen_DEED_pdf_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=DEED', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("DEED").get("date_from")}', f'--date_to={params.get("DEED").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    bergen_NS_pdf_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-NS-pdf-Scraper',
        task_id =f'bergen_NS_pdf_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=NS', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("NS").get("date_from")}', f'--date_to={params.get("NS").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    bergen_MTG_pdf_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-MTG-pdf-Scraper',
        task_id =f'bergen_MTG_pdf_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=MTG', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("MTG").get("date_from")}', f'--date_to={params.get("MTG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    bergen_SAT_pdf_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-SAT-pdf-Scraper',
        task_id =f'bergen_SAT_pdf_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=SAT', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("SAT").get("date_from")}', f'--date_to={params.get("SAT").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    bergen_ASM_pdf_scraper = KubernetesPodOperator(
        resources=resources,
        namespace = namespace,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Bergen-ASM-pdf-Scraper',
        task_id =f'bergen_ASM_pdf_scraper',
        image=f"{cr_registry}/bergen",
        arguments=['./scraper/run.py', f'--doc_type=ASM', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("ASM").get("date_from")}', f'--date_to={params.get("ASM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    start >> bergen_DEED_data_scraper >> bergen_NS_data_scraper >> bergen_MTG_data_scraper >> bergen_SAT_data_scraper >> bergen_ASM_data_scraper >> bergen_DEED_pdf_scraper >> bergen_NS_pdf_scraper >> bergen_MTG_pdf_scraper >> bergen_SAT_pdf_scraper >> bergen_ASM_pdf_scraper