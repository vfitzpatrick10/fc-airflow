from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 2,
    'retry_delay': timedelta(minutes=3)
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='lee-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval='0 0 * * 2,4,6',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=2,
    max_active_runs=1
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    lee_DEED_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-DEED-data-Scraper',
        task_id =f'lee_DEED_data_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DEED', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_MTG_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-MTG-data-Scraper',
        task_id =f'lee_MTG_data_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_MTGT_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-MTGT-data-Scraper',
        task_id =f'lee_MTGT_data_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTGT', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_RLS_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-RLS-data-Scraper',
        task_id =f'lee_RLS_data_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLS', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_LP_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-LP-data-Scraper',
        task_id =f'lee_LP_data_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=LP', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_SAT_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-SAT-data-Scraper',
        task_id =f'lee_SAT_data_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=SAT', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_ASG_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-ASG-data-Scraper',
        task_id =f'lee_ASG_data_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=ASG', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_DEED_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-DEED-pdf-Scraper',
        task_id =f'lee_DEED_pdf_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DEED', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_MTG_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-MTG-pdf-Scraper',
        task_id =f'lee_MTG_pdf_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_MTGT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-MTGT-pdf-Scraper',
        task_id =f'lee_MTGT_pdf_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTGT', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_RLS_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-RLS-pdf-Scraper',
        task_id =f'lee_RLS_pdf_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLS', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_LP_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-LP-pdf-Scraper',
        task_id =f'lee_LP_pdf_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=LP', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_SAT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-SAT-pdf-Scraper',
        task_id =f'lee_SAT_pdf_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=SAT', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_ASG_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'lee-ASG-pdf-Scraper',
        task_id =f'lee_ASG_pdf_scraper',
        image=f"{cr_registry}/lee-county",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=ASG', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=pod.small_resources(),
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        is_delete_operator_pod=True
    )

    lee_to_staged = KubernetesPodOperator(
        namespace = namespace,
        name = 'lee-To-Staged',
        task_id ='lee-to-staged',
        image=f"{cr_registry}/lee-county",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        resources=pod.small_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["./data_process/lee_to_staged.py"]
    )

    lee_staged_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'lee-Staged-To-DB',
        task_id ='lee-staged-to-db',
        image=f"{cr_registry}/forecasa-transformations",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        resources=pod.small_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["--county=lee"]
    )


    start >> [lee_DEED_data_scraper, lee_MTG_data_scraper, lee_RLS_data_scraper, lee_LP_data_scraper, lee_SAT_data_scraper, lee_MTGT_data_scraper, lee_ASG_data_scraper] >> lee_to_staged >> lee_staged_to_db
    start >> lee_DEED_pdf_scraper >> lee_MTG_pdf_scraper >> lee_RLS_pdf_scraper >> lee_LP_pdf_scraper >> lee_SAT_pdf_scraper >> lee_MTGT_pdf_scraper >> lee_ASG_pdf_scraper