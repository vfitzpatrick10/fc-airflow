from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "13Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='burlington-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval='0 14 * * 1,3,5,7',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=1
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    DOC_TYPE = {
    'DEED': 'DEED',
    'NSM': 'NOTICE OF SETTLEMENT',
    'MTG': 'MORTGAGE',
    'RLM': 'RELEASE OF MORTGAGE',
    'CNM': 'CANCELLATION OF MORTGAGE',
    'ASM': 'ASSIGNMENT OF MORTGAGE',
    }

    burlington_DEED_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-data-DEED-Scraper',
        task_id =f'burlington_data_DEED_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=DEED', '--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_NSM_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-data-NSM-Scraper',
        task_id =f'burlington_data_NSM_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=NSM', '--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_MTG_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-data-MTG-Scraper',
        task_id =f'burlington_data_MTG_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=MTG', '--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_RLM_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-data-RLM-Scraper',
        task_id =f'burlington_data_RLM_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=RLM', '--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_CNM_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-data-CNM-Scraper',
        task_id =f'burlington_data_CNM_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=CNM', '--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_ASM_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-data-ASM-Scraper',
        task_id =f'burlington_data_ASM_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=ASM', '--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_DEED_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-pdf-DEED-Scraper',
        task_id =f'burlington_pdf_DEED_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=DEED', '--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_MTG_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-pdf-MTG-Scraper',
        task_id =f'burlington_pdf_MTG_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=MTG', '--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_NSM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-pdf-NSM-Scraper',
        task_id =f'burlington_pdf_NSM_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=NSM', '--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_RLM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-pdf-RLM-Scraper',
        task_id =f'burlington_pdf_RLM_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=RLM', '--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_CNM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-pdf-CNM-Scraper',
        task_id =f'burlington_pdf_CNM_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=CNM', '--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_ASM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-pdf-ASM-Scraper',
        task_id =f'burlington_pdf_ASM_scraper',
        image=f"{cr_registry}/burlington",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=ASM', '--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_to_staged = KubernetesPodOperator(
        namespace = namespace,
        name = 'Burlington-To-Staged',
        task_id ='burlington-to-staged',
        image=f"{cr_registry}/burlington",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["./data_process/burlington_to_staged.py"]
    )

    burlington_staged_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'burlington-Staged-To-DB',
        task_id ='burlington-staged-to-db',
        image=f"{cr_registry}/forecasa-transformations",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["--county=burlington"]
    )

    start >> [burlington_DEED_data_scraper, burlington_MTG_data_scraper, burlington_NSM_data_scraper, burlington_RLM_data_scraper, burlington_CNM_data_scraper, burlington_ASM_data_scraper,
    burlington_DEED_pdf_scraper, burlington_MTG_pdf_scraper, burlington_NSM_pdf_scraper, burlington_RLM_pdf_scraper, burlington_CNM_pdf_scraper, burlington_ASM_pdf_scraper] >> burlington_to_staged >> burlington_staged_to_db