from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='mecklenburg-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval='0 17 * * 2,4,6',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=2,
    max_active_runs=1
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    DOC_TYPE = {
    'DEED': 'DEED',
    'DT': 'DEED OF TRUST',
    'SAT': 'SATISFACTION',
    'MORTGAGE': 'MORTGAGE',
    'ASM': 'ASSIGNMENT',
    }

    mecklenburg_DEED_data_scraper = KubernetesPodOperator(
        resources=pod.small_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        namespace = namespace,
        name = f'Mecklenburg-DEED-data-Scraper',
        task_id =f'mecklenburg_DEED_data_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=DEED', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_DEED_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Mecklenburg-DEED-pdf-Scraper',
        task_id =f'mecklenburg_DEED_pdf_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=DEED', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=pod.large_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_DT_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        resources=pod.small_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Mecklenburg-DT-data-Scraper',
        task_id =f'mecklenburg_DT_data_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=DT', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_DT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Mecklenburg-DT-pdf-Scraper',
        task_id =f'mecklenburg_DT_pdf_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=DT', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=pod.large_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_SAT_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        resources=pod.small_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Mecklenburg-SAT-data-Scraper',
        task_id =f'mecklenburg_SAT_data_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=SAT', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_SAT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Mecklenburg-SAT-pdf-Scraper',
        task_id =f'mecklenburg_SAT_pdf_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=SAT', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=pod.large_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_MORTGAGE_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        resources=pod.small_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Mecklenburg-MORTGAGE-data-Scraper',
        task_id =f'mecklenburg_MORTGAGE_data_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=MTG', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_MORTGAGE_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Mecklenburg-MORTGAGE-pdf-Scraper',
        task_id =f'mecklenburg_MORTGAGE_pdf_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=MTG', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=pod.large_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_ASSIGNMENT_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        resources=pod.small_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        name = f'Mecklenburg-ASSIGNMENT-data-Scraper',
        task_id =f'mecklenburg_ASSIGNMENT_data_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=ASM', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_ASSIGNMENT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Mecklenburg-ASSIGNMENT-pdf-Scraper',
        task_id =f'mecklenburg_ASSIGNMENT_pdf_scraper',
        image=f"{cr_registry}/mecklenburg",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type=ASM', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=pod.large_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        execution_timeout=timedelta(hours=10),
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'}
    )

    mecklenburg_to_staged = KubernetesPodOperator(
        namespace = namespace,
        name = 'mecklenburg-To-Staged',
        task_id ='mecklenburg-to-staged',
        image=f"{cr_registry}/mecklenburg",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        resources=pod.small_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["./data_process/mecklenburg_to_staged.py"]
    )

    mecklenburg_staged_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'mecklenburg-Staged-To-DB',
        task_id ='mecklenburg-staged-to-db',
        image=f"{cr_registry}/forecasa-transformations",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        labels={'project':'data-acquisition'},
        resources=pod.small_resources(),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["--county=mecklenburg"]
    )


    start >> [mecklenburg_DEED_data_scraper, mecklenburg_DEED_pdf_scraper, mecklenburg_DT_data_scraper, mecklenburg_DT_pdf_scraper, mecklenburg_SAT_data_scraper, mecklenburg_SAT_pdf_scraper, mecklenburg_MORTGAGE_data_scraper, mecklenburg_MORTGAGE_pdf_scraper, mecklenburg_ASSIGNMENT_data_scraper, mecklenburg_ASSIGNMENT_pdf_scraper] >> mecklenburg_to_staged >> mecklenburg_staged_to_db