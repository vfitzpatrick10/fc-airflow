from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "13Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='harris-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval='0 8 * * 1,3,5,7',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=1
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    harris_DEED_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'harris-DEED-data-Scraper',
        task_id =f'harris_DEED_data_scraper',
        image=f"{cr_registry}/harris",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=DEED', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    harris_DT_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'harris-DT-data-Scraper',
        task_id =f'harris_DT_data_scraper',
        image=f"{cr_registry}/harris",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=DT', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    harris_RLS_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'harris-RLS-data-Scraper',
        task_id =f'harris_RLS_data_scraper',
        image=f"{cr_registry}/harris",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=RLS', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    harris_ASM_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'harris-ASM-data-Scraper',
        task_id =f'harris_ASM_data_scraper',
        image=f"{cr_registry}/harris",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=ASM', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    harris_WD_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'harris-WD-data-Scraper',
        task_id =f'harris_WD_data_scraper',
        image=f"{cr_registry}/harris",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=WD', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    harris_DEED_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'harris-DEED-pdf-Scraper',
        task_id =f'harris_DEED_pdf_scraper',
        image=f"{cr_registry}/harris",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=DEED', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    harris_DT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'harris-DT-pdf-Scraper',
        task_id =f'harris_DT_pdf_scraper',
        image=f"{cr_registry}/harris",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=DT', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    harris_ASM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'harris-ASM-pdf-Scraper',
        task_id =f'harris_ASM_pdf_scraper',
        image=f"{cr_registry}/harris",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=ASM', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    harris_WD_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'harris-WD-pdf-Scraper',
        task_id =f'harris_WD_pdf_scraper',
        image=f"{cr_registry}/harris",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=WD', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    harris_to_staged = KubernetesPodOperator(
        namespace = namespace,
        name = 'harris-To-Staged',
        task_id ='harris-to-staged',
        image=f"{cr_registry}/harris",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["./data_process/harris_to_staged.py"]
    )

    harris_staged_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'harris-Staged-To-DB',
        task_id ='harris-staged-to-db',
        image=f"{cr_registry}/forecasa-transformations",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["--county=harris"]
    )


    start >> [harris_DEED_data_scraper, harris_DT_data_scraper, harris_RLS_data_scraper, harris_ASM_data_scraper, harris_WD_data_scraper] >> harris_to_staged >> harris_staged_to_db
    start >> harris_DEED_pdf_scraper >> harris_DT_pdf_scraper >> harris_ASM_pdf_scraper >> harris_WD_pdf_scraper  