import os
import pandas as pd
from builtins import range
from datetime import datetime, timedelta
from functools import partial
import itertools
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook


def on_failure_callback(context):
    webhook_token = BaseHook.get_connection('airflow-prd-slack').password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id='airflow-prd-slack',
        webhook_token=webhook_token,
        message=f""":x: { context['dag'].dag_id} Failed """,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)

def on_success_callback(context):
    webhook_token = BaseHook.get_connection('airflow-prd-slack').password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id='airflow-prd-slack',
        webhook_token=webhook_token,
        message=f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "13Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'historical'

slack_conn_id = 'airflow-prd-slack'
slack_success = partial(slack.message, status='success', conn_id=slack_conn_id)
slack_fail = partial(slack.message, status='failure', conn_id=slack_conn_id)

with DAG(
    dag_id='remedy-scraper-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    concurrency=1,
    max_active_runs=1
) as dag:

    for index, row in pd.read_csv(f"{os.getenv('AIRFLOW__CORE__DAGS_FOLDER')}/data/scraper_remedy.csv").iterrows():
        county = row['County']
        transaction_type = row['Transaction_Type']
        recorded_date = row['FC_Recorded_Date']
        pdf_scrapers = KubernetesPodOperator(
            namespace = namespace,
            name = f'{county}-historical-{transaction_type}-{recorded_date}-pdf-Scraper',
            task_id =f'{county}_historical_{transaction_type}_{recorded_date}_pdf_scraper',
            image=f"{cr_registry}/{county}",
            arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type={transaction_type}', f'--scrape_type=pdf','--historic_run', f'--date_from={recorded_date}', f'--date_to={recorded_date}'],
            image_pull_secrets='gitlabcr',
            image_pull_policy='Always',
            tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
            resources=resources,
            execution_timeout=timedelta(hours=5),
            in_cluster=True,
        startup_timeout_seconds=600,
            get_logs=False,
            secrets=pod_all_secrets
        )
        pdf_scrapers


    