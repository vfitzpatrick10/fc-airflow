from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.providers.airbyte.operators.airbyte import AirbyteTriggerSyncOperator

with DAG(dag_id='airbyte',
         default_args={'owner': 'airflow'},
         schedule_interval=None,
         start_date=days_ago(1)
    ) as dag:

    fc_web_to_db = AirbyteTriggerSyncOperator(
        task_id='fc_web_to_db',
        airbyte_conn_id='airbyte_conn',
        connection_id='81b9c2f0-1f0a-4e05-8061-b89cbc9802a4',
        asynchronous=False,
        wait_seconds=3
    )