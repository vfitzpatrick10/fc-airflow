from datetime import datetime, timedelta

from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.models import DAG
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from airflow.operators.python_operator import PythonOperator
from common.sqlalchemy_engine import start_engine
from airflow.hooks.base_hook import BaseHook


import utils.slack as slack
from utils.queries import unscrpaped_query
from utils.running_sensor import ExternalTaskRunningSensor


def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 2,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "13Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}

big_resources = {
    "request_memory": "10Gi",
    "limit_memory": "13Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}


cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'
slack_conn_id = 'airflow-prd-slack'

county = 'travis'
task_ids = f'get_{county}_unscraped'
key = f'{county}_unscraped_data'
env = 'postgresql'
concurrent_tasks = 2
 
def get_unscraped_data(**kwargs):
    county = kwargs['county']
    ti = kwargs['ti']
    env = kwargs['env']
    concurrent_tasks = kwargs['concurrent_tasks']
    session = start_engine(env)
    
    results = session.execute(unscrpaped_query.format(county=county.title())).fetchall()
    session.close()

    
    data = [{
        'doc_type': results[i][1],
        'scrape_date': str(results[i][2]),
        'scrape_type': results[i][3]
    } for i in range(concurrent_tasks)]

    ti.xcom_push(key=f'{county}_unscraped_data', value=data)

with DAG(
    dag_id=f'{county}-auto-remedy-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=1,
    max_active_runs=1
) as dag:
    
    start = slack.message('start', slack_conn_id, dag)

    get_unscraped_task = PythonOperator(
        task_id=f'get_{county}_unscraped',
        python_callable=get_unscraped_data,
        op_kwargs={'county': county, 'env': env, 'concurrent_tasks': concurrent_tasks},
        provide_context=True
    )
    
    running_sensor = ExternalTaskRunningSensor(
        task_id=f'{county}_running_sensor',
        external_dag_id=f'{county}-pipeline',
        external_task_id=None,
        mode='reschedule',
        poke_interval=60 * 10
    )
    
    pdf_scrapers = [
            KubernetesPodOperator(
        namespace = namespace,
        name = f'{county}-{i}-historical-Scraper',
        task_id =f'{county}_historical_scraper',
        image=f"{cr_registry}/{county}",
        arguments=['./scraper/run.py', '--max_retries=1', '--historic_run', f'--date_from={{{{task_instance.xcom_pull(task_ids="{task_ids}", key="{key}")[{i}]["scrape_date"]}}}}', f'--date_to={{{{task_instance.xcom_pull(task_ids="{task_ids}", key="{key}")[{i}]["scrape_date"]}}}}', f'--doc_type={{{{task_instance.xcom_pull(task_ids="{task_ids}", key="{key}")[{i}]["doc_type"]}}}}', f'--scrape_type={{{{task_instance.xcom_pull(task_ids="{task_ids}", key="{key}")[{i}]["scrape_type"]}}}}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False
    ) for i in range(concurrent_tasks)
    ]
    
    trigger = TriggerDagRunOperator(
        task_id = 'trigger_rerun',
        trigger_dag_id='{county}-auto-remedy-pipeline'
    )
    
    
    start >> get_unscraped_task >> running_sensor >> pdf_scrapers >> trigger


    