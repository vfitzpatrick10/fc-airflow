from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "6Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

with DAG(
    dag_id='allegheny-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval='0 6 * * 1,3,5,7',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=1
) as dag:

    start = slack.message('start', 'airflow-prd-slack', dag)

    DOC_TYPE = {
    'DEED': 'DEED',
    'MTG': 'MORTGAGE',
    'SAT': 'SATISFACTION OF MORTGAGE',
    'ASM': 'ASSIGNMENT OF MORTGAGE',
    }

    allegheny_scrapers = [KubernetesPodOperator(
        namespace = namespace,
        name = f'allegheny-{doc_type}-Scraper',
        task_id =f'allegheny_{doc_type}_scraper',
        image=f"{cr_registry}/allegheny-county",
        arguments=['./scraper/run.py', '--max_retries=2', f'--doc_type={doc_type}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        execution_timeout=timedelta(hours=10),
        resources=resources,
        labels={'project':'data-acquisition'},
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        secrets=pod_all_secrets
    ) for doc_type in DOC_TYPE.keys()]

    allegheny_to_staged = KubernetesPodOperator(
        namespace = namespace,
        name = 'allegheny-To-Staged',
        task_id ='allegheny-to-staged',
        image=f"{cr_registry}/allegheny-county",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["./data_process/allegheny_to_staged.py"],
        labels={'project':'data-acquisition'},
        secrets=pod_all_secrets
    )

    allegheny_staged_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'allegheny-Staged-To-DB',
        task_id ='allegheny-staged-to-db',
        image=f"{cr_registry}/forecasa-transformations",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["--county=allegheny"],
        labels={'project':'data-acquisition'},
        secrets=pod_all_secrets
    )

    start >> allegheny_scrapers >> allegheny_to_staged >> allegheny_staged_to_db