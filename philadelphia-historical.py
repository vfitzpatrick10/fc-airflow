from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

def get_historic_params(county):
    params = Variable.get('historical', deserialize_json=True)
    return params.get(county)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "6Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'historical'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='philadelphia-historical-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=2,
    max_active_runs=2
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    params = get_historic_params('philadelphia')    
    
    philadelphia_DEED_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-DEED-data-Scraper-historical',
        task_id =f'philadelphia_DEED_data_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DEED', f'--scrape_type=data', '--historic_run', f'--date_from={params.get("DEED").get("date_from")}', f'--date_to={params.get("DEED").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_DEED_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-DEED-pdf-Scraper-historical',
        task_id =f'philadelphia_DEED_pdf_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DEED', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("DEED").get("date_from")}', f'--date_to={params.get("DEED").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_DS_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-DS-data-Scraper-historical',
        task_id =f'philadelphia_DS_data_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DS', f'--scrape_type=data', '--historic_run', f'--date_from={params.get("DS").get("date_from")}', f'--date_to={params.get("DS").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_DS_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-DS-pdf-Scraper-historical',
        task_id =f'philadelphia_DS_pdf_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DS', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("DS").get("date_from")}', f'--date_to={params.get("DS").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_SAT_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-SAT-data-Scraper-historical',
        task_id =f'philadelphia_SAT_data_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=SAT', f'--scrape_type=data', '--historic_run', f'--date_from={params.get("SAT").get("date_from")}', f'--date_to={params.get("SAT").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_SAT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-SAT-pdf-Scraper-historical',
        task_id =f'philadelphia_SAT_pdf_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=SAT', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("SAT").get("date_from")}', f'--date_to={params.get("SAT").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_MTG_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-MTG-data-Scraper-historical',
        task_id =f'philadelphia_MTG_data_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=data', '--historic_run', f'--date_from={params.get("MTG").get("date_from")}', f'--date_to={params.get("MTG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_MTG_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-MTG-pdf-Scraper-historical',
        task_id =f'philadelphia_MTG_pdf_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("MTG").get("date_from")}', f'--date_to={params.get("MTG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_RLS_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-RLS-data-Scraper-historical',
        task_id =f'philadelphia_RLS_data_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLS', f'--scrape_type=data', '--historic_run', f'--date_from={params.get("REL").get("date_from")}', f'--date_to={params.get("REL").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_RLS_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-RLS-pdf-Scraper-historical',
        task_id =f'philadelphia_RLS_pdf_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLS', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("REL").get("date_from")}', f'--date_to={params.get("REL").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_ASM_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-ASM-data-Scraper-historical',
        task_id =f'philadelphia_ASM_data_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=ASM', f'--scrape_type=data', '--historic_run', f'--date_from={params.get("ASM").get("date_from")}', f'--date_to={params.get("ASM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_ASM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-ASM-pdf-Scraper-historical',
        task_id =f'philadelphia_ASM_pdf_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=ASM', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("ASM").get("date_from")}', f'--date_to={params.get("ASM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_DM_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-DM-data-Scraper-historical',
        task_id =f'philadelphia_DM_data_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DM', f'--scrape_type=data', '--historic_run', f'--date_from={params.get("DM").get("date_from")}', f'--date_to={params.get("DM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    philadelphia_DM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Philadelphia-DM-pdf-Scraper-historical',
        task_id =f'philadelphia_DM_pdf_scraper_historical',
        image=f"{cr_registry}/philadelphia",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DM', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("DM").get("date_from")}', f'--date_to={params.get("DM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    start >> philadelphia_DEED_data_scraper >> philadelphia_DS_data_scraper >> philadelphia_MTG_data_scraper >> philadelphia_SAT_data_scraper >> philadelphia_RLS_data_scraper >> philadelphia_ASM_data_scraper >> philadelphia_DM_data_scraper
    start >> philadelphia_DEED_pdf_scraper >> philadelphia_DS_pdf_scraper >> philadelphia_MTG_pdf_scraper >>philadelphia_SAT_pdf_scraper >> philadelphia_RLS_pdf_scraper >> philadelphia_ASM_pdf_scraper >> philadelphia_DM_pdf_scraper     
