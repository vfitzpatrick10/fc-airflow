from builtins import range
from datetime import datetime, timedelta
from functools import partial
import itertools
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_failure_callback(context):
    webhook_token = BaseHook.get_connection('airflow-prd-slack').password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id='airflow-prd-slack',
        webhook_token=webhook_token,
        message=f""":x: { context['dag'].dag_id} Failed """,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)


def on_success_callback(context):
    webhook_token = BaseHook.get_connection('airflow-prd-slack').password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id='airflow-prd-slack',
        webhook_token=webhook_token,
        message=f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'
slack_success = partial(slack.message, status='success', conn_id=slack_conn_id)
slack_fail = partial(slack.message, status='failure', conn_id=slack_conn_id)

with DAG(
    dag_id='csv-backup',
    default_args=args,    
    catchup=False,
    schedule_interval='0 18 * * *',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=2
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    counties = ['travis', 'bergen', 'allegheny']

    cvs_backups = [KubernetesPodOperator(
        namespace = namespace,
        name = f'{county}-csv-backup',
        task_id =f'{county}-csv-backup',
        image=f"{cr_registry}/file-mover",
        arguments=['--operation=backup', '--filetype=csv', f'--county={county}', f'--start_date=2020-11-20', '--bucket=testdata'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=True,
        secrets=pod_all_secrets
    ) for county in counties]

   

    start >> cvs_backups