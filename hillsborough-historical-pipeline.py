from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

def get_historic_params(county):
    params = Variable.get('historical', deserialize_json=True)
    return params.get(county)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "12Gi",
    "limit_memory": "13Gi",
    "request_cpu": "3000m",
    "limit_cpu": "3500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'historical'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='hillsborough-historical-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=2
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    params = get_historic_params('hillsborough')

    hillsborough_DEED_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-DEED-pdf-Scraper-historical',
        task_id =f'hillsborough_DEED_pdf_scraper_historical',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DEED', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("DEED").get("date_from")}', f'--date_to={params.get("DEED").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_MTG_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-MTG-pdf-Scraper-historical',
        task_id =f'hillsborough_MTG_pdf_scraper_historical',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("MTG").get("date_from")}', f'--date_to={params.get("MTG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_SAT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-SAT-pdf-Scraper-historical',
        task_id =f'hillsborough_SAT_pdf_scraper_historical',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=SAT', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("SAT").get("date_from")}', f'--date_to={params.get("SAT").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_RLS_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-RLS-pdf-Scraper-historical',
        task_id =f'hillsborough_RLS_pdf_scraper_historical',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("RLS").get("date_from")}', f'--date_to={params.get("RLS").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_ASM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-ASM-pdf-Scraper-historical',
        task_id =f'hillsborough_ASM_pdf_scraper_historical',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("ASM").get("date_from")}', f'--date_to={params.get("ASM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_RLP_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-RLP-pdf-Scraper-historical',
        task_id =f'hillsborough_RLP_pdf_scraper_historical',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("RLP").get("date_from")}', f'--date_to={params.get("RLP").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_MOD_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-MOD-pdf-Scraper-historical',
        task_id =f'hillsborough_MOD_pdf_scraper_historical',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("MOD").get("date_from")}', f'--date_to={params.get("MOD").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_NSC_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-NSC-pdf-Scraper-historical',
        task_id =f'hillsborough_NSC_pdf_scraper_historical',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("NSC").get("date_from")}', f'--date_to={params.get("NSC").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_LP_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-LP-pdf-Scraper-historical',
        task_id =f'hillsborough_LP_pdf_scraper_historical',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("LP").get("date_from")}', f'--date_to={params.get("LP").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )


start >> hillsborough_DEED_pdf_scraper >> hillsborough_MTG_pdf_scraper >> hillsborough_ASM_pdf_scraper >> hillsborough_RLS_pdf_scraper >> hillsborough_LP_pdf_scraper >> hillsborough_RLP_pdf_scraper >> hillsborough_MOD_pdf_scraper >> hillsborough_SAT_pdf_scraper >> hillsborough_NSC_pdf_scraper
