from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

def get_historic_params(county):
    params = Variable.get('historical', deserialize_json=True)
    return params.get(county)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "13Gi",
    "request_cpu": "100m",
    "limit_cpu": "500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='burlington-historical-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=2
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    params = get_historic_params('burlington')

    DOC_TYPE = {
    'DEED': 'DEED',
    'NSM': 'NOTICE OF SETTLEMENT',
    'MTG': 'MORTGAGE',
    'RLM': 'RELEASE OF MORTGAGE',
    'CNM': 'CANCELLATION OF MORTGAGE',
    'ASM': 'ASSIGNMENT OF MORTGAGE',
    }

    burlington_DEED_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-DEED-pdf-Scraper-historical',
        task_id =f'burlington_DEED_pdf_scraper_historical',
        image=f"{cr_registry}//burlington-scraper",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DEED', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("DEED").get("date_from")}', f'--date_to={params.get("DEED").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_NSM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-NSM-pdf-Scraper-historical',
        task_id =f'burlington_NSM_pdf_scraper_historical',
        image=f"{cr_registry}//burlington-scraper",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=NSM', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("NSM").get("date_from")}', f'--date_to={params.get("NSM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_RLM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-RLM-pdf-Scraper-historical',
        task_id =f'burlington_RLM_pdf_scraper_historical',
        image=f"{cr_registry}//burlington-scraper",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLM', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("RLM").get("date_from")}', f'--date_to={params.get("RLM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_MORTGAGE_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-MORTGAGE-pdf-Scraper-historical',
        task_id =f'burlington_MORTGAGE_pdf_scraper_historical',
        image=f"{cr_registry}//burlington-scraper",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("MTG").get("date_from")}', f'--date_to={params.get("MTG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_CNM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-CNM-pdf-Scraper-historical',
        task_id =f'burlington_CNM_pdf_scraper_historical',
        image=f"{cr_registry}//burlington-scraper",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=CNM', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("CNM").get("date_from")}', f'--date_to={params.get("CNM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    burlington_ASM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'Burlington-ASM-pdf-Scraper-historical',
        task_id =f'burlington_ASM_pdf_scraper_historical',
        image=f"{cr_registry}//burlington-scraper",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=ASM', f'--scrape_type=pdf', '--historic_run', f'--date_from={params.get("ASM").get("date_from")}', f'--date_to={params.get("ASM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    start >> burlington_DEED_pdf_scraper >> burlington_NSM_pdf_scraper >> burlington_RLM_pdf_scraper >> burlington_MORTGAGE_pdf_scraper >> burlington_CNM_pdf_scraper >> burlington_ASM_pdf_scraper