from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 2,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "13Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='hillsborough-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval='0 0 * * 1,3,5,7',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=1
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    hillsborough_DEED_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-DEED-data-Scraper',
        task_id =f'hillsborough_DEED_data_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DEED', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_MTG_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-MTG-data-Scraper',
        task_id =f'hillsborough_MTG_data_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_ASM_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-ASM-data-Scraper',
        task_id =f'hillsborough_ASM_data_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=ASM', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_RLS_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-RLS-data-Scraper',
        task_id =f'hillsborough_RLS_data_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLS', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_LP_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-LP-data-Scraper',
        task_id =f'hillsborough_LP_data_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=LP', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_RLP_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-RLP-data-Scraper',
        task_id =f'hillsborough_RLP_data_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLP', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_MOD_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-MOD-data-Scraper',
        task_id =f'hillsborough_MOD_data_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MOD', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_SAT_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-SAT-data-Scraper',
        task_id =f'hillsborough_SAT_data_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=SAT', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_NSC_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-NSC-data-Scraper',
        task_id =f'hillsborough_NSC_data_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=NSC', f'--scrape_type=data'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_DEED_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-DEED-pdf-Scraper',
        task_id =f'hillsborough_DEED_pdf_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DEED', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_MTG_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-MTG-pdf-Scraper',
        task_id =f'hillsborough_MTG_pdf_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_ASM_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-ASM-pdf-Scraper',
        task_id =f'hillsborough_ASM_pdf_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=ASM', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_RLS_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-RLS-pdf-Scraper',
        task_id =f'hillsborough_RLS_pdf_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLS', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_LP_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-LP-pdf-Scraper',
        task_id =f'hillsborough_LP_pdf_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=LP', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_RLP_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-RLP-pdf-Scraper',
        task_id =f'hillsborough_RLP_pdf_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLP', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_MOD_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-MOD-pdf-Scraper',
        task_id =f'hillsborough_MOD_pdf_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MOD', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_SAT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-SAT-pdf-Scraper',
        task_id =f'hillsborough_SAT_pdf_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=SAT', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    hillsborough_NSC_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'hillsborough-NSC-pdf-Scraper',
        task_id =f'hillsborough_NSC_pdf_scraper',
        image=f"{cr_registry}/hillsborough",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=NSC', f'--scrape_type=pdf'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        execution_timeout=timedelta(hours=10),
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )


    hillsborough_to_staged = KubernetesPodOperator(
        namespace = namespace,
        name = 'hillsborough-To-Staged',
        task_id ='hillsborough-to-staged',
        image=f"{cr_registry}/hillsborough",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["./data_process/hillsborough_to_staged.py"]
    )

    hillsborough_staged_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'hillsborough-Staged-To-DB',
        task_id ='hillsborough-staged-to-db',
        image=f"{cr_registry}/forecasa-transformations",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["--county=hillsborough"]
    )


    start >> [hillsborough_DEED_data_scraper,  hillsborough_MTG_data_scraper, hillsborough_ASM_data_scraper, hillsborough_RLS_data_scraper, hillsborough_LP_data_scraper, hillsborough_RLP_data_scraper, hillsborough_MOD_data_scraper, hillsborough_SAT_data_scraper, hillsborough_NSC_data_scraper] >> hillsborough_to_staged >> hillsborough_staged_to_db
    start >> hillsborough_DEED_pdf_scraper >> hillsborough_MTG_pdf_scraper >> hillsborough_ASM_pdf_scraper >> hillsborough_RLS_pdf_scraper >> hillsborough_LP_pdf_scraper >> hillsborough_RLP_pdf_scraper >> hillsborough_MOD_pdf_scraper >> hillsborough_SAT_pdf_scraper >> hillsborough_NSC_pdf_scraper
