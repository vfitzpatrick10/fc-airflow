from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

def get_historic_params(county):
    params = Variable.get('historical', deserialize_json=True)
    return params.get(county)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 2,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "13Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'historical'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='palm_beach-historical-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=1
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    params = get_historic_params('palm-beach')

    palm_beach_DEED_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-DEED-data-Scraper',
        task_id =f'palm_beach_historical_DEED_data_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=DEED', f'--scrape_type=data','--historic_run', f'--date_from={params.get("DEED").get("date_from")}', f'--date_to={params.get("DEED").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_MTG_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-MTG-data-Scraper',
        task_id =f'palm_beach_historical_MTG_data_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=MTG', f'--scrape_type=data','--historic_run', f'--date_from={params.get("MTG").get("date_from")}', f'--date_to={params.get("MTG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_JUD_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-JUD-data-Scraper',
        task_id =f'palm_beach_historical_JUD_data_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=JUD', f'--scrape_type=data','--historic_run', f'--date_from={params.get("JUD").get("date_from")}', f'--date_to={params.get("JUD").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_RLS_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-RLS-data-Scraper',
        task_id =f'palm_beach_historical_RLS_data_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=RLS', f'--scrape_type=data','--historic_run', f'--date_from={params.get("RLS").get("date_from")}', f'--date_to={params.get("RLS").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_LP_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-LP-data-Scraper',
        task_id =f'palm_beach_historical_LP_data_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=LP', f'--scrape_type=data','--historic_run', f'--date_from={params.get("LP").get("date_from")}', f'--date_to={params.get("LP").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_SAT_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-SAT-data-Scraper',
        task_id =f'palm_beach_historical_SAT_data_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=SAT', f'--scrape_type=data','--historic_run', f'--date_from={params.get("SAT").get("date_from")}', f'--date_to={params.get("SAT").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_ASG_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-ASG-data-Scraper',
        task_id =f'palm_beach_historical_ASG_data_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=ASG', f'--scrape_type=data','--historic_run', f'--date_from={params.get("ASG").get("date_from")}', f'--date_to={params.get("ASG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_TER_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-TER-data-Scraper',
        task_id =f'palm_beach_historical_TER_data_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=TER', f'--scrape_type=data','--historic_run', f'--date_from={params.get("TER").get("date_from")}', f'--date_to={params.get("TER").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_DEED_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-DEED-pdf-Scraper',
        task_id =f'palm_beach_historical_DEED_pdf_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=DEED', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("DEED").get("date_from")}', f'--date_to={params.get("DEED").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_MTG_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-MTG-pdf-Scraper',
        task_id =f'palm_beach_historical_MTG_pdf_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=MTG', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("MTG").get("date_from")}', f'--date_to={params.get("MTG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_JUD_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-JUD-pdf-Scraper',
        task_id =f'palm_beach_historical_JUD_pdf_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=JUD', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("JUD").get("date_from")}', f'--date_to={params.get("JUD").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_RLS_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-RLS-pdf-Scraper',
        task_id =f'palm_beach_historical_RLS_pdf_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=RLS', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("RLS").get("date_from")}', f'--date_to={params.get("RLS").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_LP_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-LP-pdf-Scraper',
        task_id =f'palm_beach_historical_LP_pdf_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=LP', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("LP").get("date_from")}', f'--date_to={params.get("LP").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_SAT_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-SAT-pdf-Scraper',
        task_id =f'palm_beach_historical_SAT_pdf_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=SAT', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("SAT").get("date_from")}', f'--date_to={params.get("SAT").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_ASG_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-ASG-pdf-Scraper',
        task_id =f'palm_beach_historical_ASG_pdf_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=ASG', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("ASG").get("date_from")}', f'--date_to={params.get("ASG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    palm_beach_TER_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'palm_beach-historical-TER-pdf-Scraper',
        task_id =f'palm_beach_historical_TER_pdf_scraper',
        image=f"{cr_registry}/palm-beach",
        arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type=TER', f'--scrape_type=pdf','--historic_run', f'--date_from={params.get("TER").get("date_from")}', f'--date_to={params.get("TER").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    start >> palm_beach_DEED_data_scraper >>  palm_beach_MTG_data_scraper >> palm_beach_JUD_data_scraper >> palm_beach_RLS_data_scraper >> palm_beach_LP_data_scraper >> palm_beach_SAT_data_scraper >> palm_beach_ASG_data_scraper >> palm_beach_TER_data_scraper >> palm_beach_DEED_pdf_scraper >> palm_beach_MTG_pdf_scraper >> palm_beach_JUD_pdf_scraper >> palm_beach_RLS_pdf_scraper >> palm_beach_LP_pdf_scraper >> palm_beach_SAT_pdf_scraper >> palm_beach_ASG_pdf_scraper >> palm_beach_TER_pdf_scraper
