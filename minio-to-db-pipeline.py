from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='minio-to-db-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval='0 18 * * *',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=2
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    counties = [
        'allegheny',
        'burlington',
        'durham',
        'mecklenburg',
        'nyc',
        'philadelphia',
        'travis',
        'bergen'
    ]
    
    allegheny_minio_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'allegheny-minio-to-db',
        task_id ='allegheny_minio_to_db',
        image=f"{cr_registry}/file-mover",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        arguments=["--operation=minio_to_db", "--county=allegheny", "--bucket=fcdata"]
    )

    burlington_minio_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'burlington-minio-to-db',
        task_id ='burlington_minio_to_db',
        image=f"{cr_registry}/file-mover",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        arguments=["--operation=minio_to_db", "--county=burlington", "--bucket=fcdata"]
    )

    durham_minio_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'durham-minio-to-db',
        task_id ='durham_minio_to_db',
        image=f"{cr_registry}/file-mover",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        arguments=["--operation=minio_to_db", "--county=durham", "--bucket=fcdata"]
    )

    mecklenburg_minio_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'mecklenburg-minio-to-db',
        task_id ='mecklenburg_minio_to_db',
        image=f"{cr_registry}/file-mover",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        arguments=["--operation=minio_to_db", "--county=mecklenburg", "--bucket=fcdata"]
    )

    nyc_minio_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'nyc-minio-to-db',
        task_id ='nyc_minio_to_db',
        image=f"{cr_registry}/file-mover",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        arguments=["--operation=minio_to_db", "--county=nyc", "--bucket=fcdata"]
    )

    philadelphia_minio_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'philadelphia-minio-to-db',
        task_id ='philadelphia_minio_to_db',
        image=f"{cr_registry}/file-mover",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        arguments=["--operation=minio_to_db", "--county=philadelphia", "--bucket=fcdata"]
    )

    travis_minio_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'travis-minio-to-db',
        task_id ='travis_minio_to_db',
        image=f"{cr_registry}/file-mover",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        arguments=["--operation=minio_to_db", "--county=travis", "--bucket=fcdata"]
    )
    bergen_minio_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'bergen-minio-to-db',
        task_id ='bergen_minio_to_db',
        image=f"{cr_registry}/file-mover",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        arguments=["--operation=minio_to_db", "--county=bergen", "--bucket=fcdata"]
    )


    start >> allegheny_minio_to_db >>  burlington_minio_to_db >> durham_minio_to_db >> mecklenburg_minio_to_db >> nyc_minio_to_db >> philadelphia_minio_to_db >> travis_minio_to_db >> bergen_minio_to_db