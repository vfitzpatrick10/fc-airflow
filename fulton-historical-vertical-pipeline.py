import os
import pandas as pd
from builtins import range
from datetime import datetime, timedelta, date
from functools import partial
import itertools
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook


def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

def get_date_params(county):
    vertical = Variable.get('historical-vertical', deserialize_json=True)
    params = vertical.get(county)
    return params.get('date_to'), params.get('date_from')

def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)+1):
        yield start_date + timedelta(n)

def chain_tasks(tasks):
    ordered_tasks = []
    for i in range(len(tasks)):
        if i>0:
            ordered_tasks[i-1] >> tasks[i]
        ordered_tasks.append(tasks[i])
    return ordered_tasks

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 2,
    'retry_delay': timedelta(minutes=3)
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'
slack_success = partial(slack.message, status='success', conn_id=slack_conn_id)
slack_fail = partial(slack.message, status='failure', conn_id=slack_conn_id)

county='fulton'
doc_types = [
    'DEED',
    'MTG',
    'RLS',
    'SAT',
    'ASG',
]

with DAG(
    dag_id=f'{county}-historical-vertical-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    concurrency=1,
    max_active_runs=1
) as dag:

    start = DummyOperator(task_id='start')

    date_to_str, date_from_str = get_date_params(county)

    start_date = datetime.strptime(date_from_str,'%Y-%m-%d')
    end_date = datetime.strptime(date_to_str,'%Y-%m-%d')

    for single_date in daterange(start_date, end_date):        
        date = single_date.strftime("%Y-%m-%d")

        data_scrapers = [
            KubernetesPodOperator(
                namespace = namespace,
                name = f'{county}-historical-{doc_type}-{date}-data-Scraper',
                task_id =f'{county}_historical_{doc_type}_{date}_data_scraper',
                image=f"{cr_registry}/{county}-county",
                arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type={doc_type}', f'--scrape_type=data','--historic_run', f'--date_from={date}', f'--date_to={date}'],
                image_pull_secrets='gitlabcr',
                image_pull_policy='Always',
                tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
                resources=pod.medium_resources(),
                secrets=pod_all_secrets,
                in_cluster=True,
        startup_timeout_seconds=600,
                get_logs=False
            ) for doc_type in doc_types
        ]
        
        pdf_scrapers = [
            KubernetesPodOperator(
                namespace = namespace,
                name = f'{county}-historical-{doc_type}-{date}-pdf-Scraper',
                task_id =f'{county}_historical_{doc_type}_{date}_pdf_scraper',
                image=f"{cr_registry}/{county}-county",
                arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type={doc_type}', f'--scrape_type=pdf','--historic_run', f'--date_from={date}', f'--date_to={date}'],
                image_pull_secrets='gitlabcr',
                image_pull_policy='Always',
                tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
                resources=pod.medium_resources(),
                secrets=pod_all_secrets,
                in_cluster=True,
        startup_timeout_seconds=600,
                get_logs=False
            ) for doc_type in doc_types
        ]

        start >> data_scrapers
        start >> pdf_scrapers

