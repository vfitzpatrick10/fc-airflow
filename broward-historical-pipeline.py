from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

def get_historic_params(county):
    params = Variable.get('historical', deserialize_json=True)
    return params.get(county)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "4Gi",
    "limit_memory": "13Gi",
    "request_cpu": "1000m",
    "limit_cpu": "1500m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'historical'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='broward-historical-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=1
) as dag:

    start = slack.message('start', slack_conn_id, dag)

    params = get_historic_params('broward')

    broward_DEED_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'broward-historical-DEED-Scraper',
        task_id =f'broward_historical_DEED_scraper',
        image=f"{cr_registry}/broward",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=DEED', '--historic_run', f'--date_from={params.get("DEED").get("date_from")}', f'--date_to={params.get("DEED").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    broward_MTG_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'broward-historical-MTG-Scraper',
        task_id =f'broward_historical_MTG_scraper',
        image=f"{cr_registry}/broward",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=MTG', '--historic_run', f'--date_from={params.get("MTG").get("date_from")}', f'--date_to={params.get("MTG").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    broward_ASM_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'broward-historical-ASM-Scraper',
        task_id =f'broward_historical_ASM_scraper',
        image=f"{cr_registry}/broward",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=ASM', '--historic_run', f'--date_from={params.get("ASM").get("date_from")}', f'--date_to={params.get("ASM").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    broward_RLS_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'broward-historical-RLS-Scraper',
        task_id =f'broward_historical_RLS_scraper',
        image=f"{cr_registry}/broward",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=RLS', '--historic_run', f'--date_from={params.get("RLS").get("date_from")}', f'--date_to={params.get("RLS").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    broward_LP_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'broward-historical-LP-Scraper',
        task_id =f'broward_historical_LP_scraper',
        image=f"{cr_registry}/broward",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=LP', '--historic_run', f'--date_from={params.get("LP").get("date_from")}', f'--date_to={params.get("LP").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    broward_FJ_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'broward-historical-FJ-Scraper',
        task_id =f'broward_historical_FJ_scraper',
        image=f"{cr_registry}/broward",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=FJ', '--historic_run', f'--date_from={params.get("FJ").get("date_from")}', f'--date_to={params.get("FJ").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    broward_AGR_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = f'broward-historical-AGR-Scraper',
        task_id =f'broward_historical_AGR_scraper',
        image=f"{cr_registry}/broward",
        arguments=['./scraper/run.py', '--max_retries=1', f'--doc_type=AGR', '--historic_run', f'--date_from={params.get("AGR").get("date_from")}', f'--date_to={params.get("AGR").get("date_to")}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        resources=resources,
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )


    start >> broward_DEED_scraper >> broward_MTG_scraper >> broward_ASM_scraper >> broward_RLS_scraper >> broward_LP_scraper >> broward_FJ_scraper >> broward_AGR_scraper