import os
import pandas as pd
from builtins import range
from datetime import datetime, timedelta, date
from functools import partial
import itertools
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook


def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 2,
    'retry_delay': timedelta(minutes=3)
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'
slack_success = partial(slack.message, status='success', conn_id=slack_conn_id)
slack_fail = partial(slack.message, status='failure', conn_id=slack_conn_id)

with DAG(
    dag_id='lee-remedy-scraper-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval=None,
    concurrency=1,
    max_active_runs=1
) as dag:

    start = DummyOperator(task_id='start')
    
    for index, row in pd.read_csv(f"{os.getenv('AIRFLOW__CORE__DAGS_FOLDER')}/data/Lee_remedy.csv").iterrows():
        transaction_type = row['Transaction_Type']
        scrape_type = row['Scrape_Type']
        county = row['County']
        recorded_date = row['FC_Recorded_Date']
        scraper = KubernetesPodOperator(
            namespace = namespace,
            name = f'{county}-historical-{transaction_type}-{recorded_date}-{scrape_type}-Scraper',
            task_id =f'{county}_historical_{transaction_type}_{recorded_date}_{scrape_type}_scraper',
            image=f"{cr_registry}/lee-county",
            arguments=['./scraper/run.py', '--max_retries=3', f'--doc_type={transaction_type}', f'--scrape_type={scrape_type}','--historic_run', f'--date_from={recorded_date}', f'--date_to={recorded_date}'],
            image_pull_secrets='gitlabcr',
            image_pull_policy='Always',
            tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
            resources=pod.small_resources(),
            execution_timeout=timedelta(hours=5),
            in_cluster=True,
        startup_timeout_seconds=600,
            get_logs=False,
            secrets=pod_all_secrets
        )
        scraper
