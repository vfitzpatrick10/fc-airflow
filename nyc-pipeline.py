from builtins import range
from datetime import datetime, timedelta
from functools import partial
import utils.slack as slack
import utils.pod as pod

import airflow
from airflow.models import DAG, Variable
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def on_callback_base(context, slack_channel, message):
    webhook_token = BaseHook.get_connection(slack_channel).password
    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=slack_channel,
        webhook_token=webhook_token,
        message=message,
        link_names=True,
        username='airflow',
    )
    alert.execute(context=context)
    
def on_success_callback(context):
    success_message = f""":heavy_check_mark: {dag.dag_id} Sucessfully Finished """
    on_callback_base(context, 'airflow-prd-slack', success_message)
    on_callback_base(context, 'airflow-prd-slack-dev', success_message)

def on_failure_callback(context):
    failure_message = f""":x: @here { context['dag'].dag_id} Failed """

    on_callback_base(context, 'airflow-prd-slack', failure_message)
    on_callback_base(context, 'airflow-prd-slack-dev', failure_message)

def get_date_to():
    base_parameters = Variable.get('nyc-scraper', deserialize_json=True)
    date_to  = base_parameters.get('date_to','date_to')

    if not date_to:
        date_to = str(datetime.now().date() - timedelta(days=5))
    
    return date_to

postgres_prd_secret = pod.get_postgres_secret(host='POSTGRESQL_PRD_HOST', dbname='POSTGRESQL_PRD_DBNAME', username='POSTGRESQL_PRD_USER', password='POSTGRESQL_PRD_PASSWORD', secret='postgresql-prod')
postgres_dev_secret = pod.get_postgres_secret(host='POSTGRESQL_DEV_HOST', dbname='POSTGRESQL_DEV_DBNAME', username='POSTGRESQL_DEV_USER', password='POSTGRESQL_DEV_PASSWORD', secret='postgresql-dev')
s3_secret = pod.get_s3_secret(access_key_id='AWS_ACCESS_KEY_ID', secret_access_key='AWS_SECRET_ACCESS_KEY', region='REGION', bucket='BUCKET')

pod_all_secrets = [*postgres_prd_secret, *postgres_dev_secret, *s3_secret]

args = {
    'owner': 'airflow',
    'start_date': datetime(2021, 8, 24, 0, 0),
    'retries': 1,
    'retry_delay': timedelta(minutes=3)
}

resources = {
    "request_memory": "9Gi",
    "limit_memory": "13Gi",
    "request_cpu": "3500m",
    "limit_cpu": "3550m"
}

cr_registry = 'registry.gitlab.com/forecasa'
namespace = 'airflow'

slack_conn_id = 'airflow-prd-slack'

with DAG(
    dag_id='nyc-pipeline',
    default_args=args,    
    catchup=False,
    schedule_interval='0 0 * * 3',
    on_success_callback=on_success_callback,
    on_failure_callback=on_failure_callback,
    concurrency=4,
    max_active_runs=1
) as dag:

    start = slack.message('start', slack_conn_id, dag)
    date_to = get_date_to()
    nyc_deed_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-DEED-data',
        task_id ='nyc-scraper-deed-data',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=DEED', '--scrape_type=data', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_deed_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-DEED-pdf',
        task_id ='nyc-scraper-deed-pdf',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=DEED', '--scrape_type=pdf', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_sat_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-SAT-data',
        task_id ='nyc-scraper-sat-data',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=SAT', '--scrape_type=data', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_sat_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-SAT-pdf',
        task_id ='nyc-scraper-sat-pdf',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=SAT', '--scrape_type=pdf', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_mtge_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-MTGE-data',
        task_id ='nyc-scraper-mtge-data',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=MTGE', '--scrape_type=data', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_mtge_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-MTGE-pdf',
        task_id ='nyc-scraper-mtge-pdf',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=MTGE', '--scrape_type=pdf', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_asm_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-ASM-data',
        task_id ='nyc-scraper-asm-data',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=ASM', '--scrape_type=data', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_asm_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-ASM-pdf',
        task_id ='nyc-scraper-asm-pdf',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=ASM', '--scrape_type=pdf', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_agm_data_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-AGM-data',
        task_id ='nyc-scraper-agm-data',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=AGM', '--scrape_type=data', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_agm_pdf_scraper = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Scraper-AGM-pdf',
        task_id ='nyc-scraper-agm-pdf',
        image=f"{cr_registry}/nyc",
        arguments=['./scraper/run.py','--max_retries=5', '--doc_type=AGM', '--scrape_type=pdf', f'--date_to={date_to}'],
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        resources=resources,
        execution_timeout=timedelta(hours=10),
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets
    )

    nyc_to_staged = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-To-Staged',
        task_id ='nyc-to-staged',
        image=f"{cr_registry}/nyc",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["./data_process/nyc_to_staged.py"]
    )

    nyc_staged_to_db = KubernetesPodOperator(
        namespace = namespace,
        name = 'NYC-Staged-To-DB',
        task_id ='nyc-staged-to-db',
        image=f"{cr_registry}/forecasa-transformations",
        image_pull_secrets='gitlabcr',
        image_pull_policy='Always',
        in_cluster=True,
        startup_timeout_seconds=600,
        get_logs=False,
        secrets=pod_all_secrets,
        resources=resources,
        tolerations=[{'key':'special','effect':'NoSchedule','value':'true'}],
        arguments=["--county=nyc"]
    )

    start >> [nyc_deed_data_scraper, nyc_sat_data_scraper, nyc_mtge_data_scraper, nyc_asm_data_scraper, nyc_agm_data_scraper, nyc_deed_pdf_scraper, nyc_sat_pdf_scraper, nyc_mtge_pdf_scraper, nyc_asm_pdf_scraper, nyc_agm_pdf_scraper] >> nyc_to_staged >> nyc_staged_to_db