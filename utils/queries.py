get_date_limit = ''' select "FC_Recorded_Date"
            from scraper_meta
            where "IsDataFinished" = true
            and "County" = '{county}'
            order by "FC_Recorded_Date" desc
            LIMIT(1)            
        '''

harris_missing_pdfs = '''select *
                from scraper_meta
                where "IsPdfFinished" = false
                and "IsDataFinished" = true
                and "County" = '{county}'
                and "Transaction_Type" != 'RLS'
                and "FC_Recorded_Date" < '{date_limit}'
                and "Doc_Count" > 0
                order by "FC_Recorded_Date" desc
                LIMIT(300)
            '''

missing_pdfs = '''select *
                from scraper_meta
                where "IsPdfFinished" = false
                and "IsDataFinished" = true
                and "County" = '{county}'
                and "FC_Recorded_Date" < '{date_limit}'
                and "Doc_Count" > 0
                order by "FC_Recorded_Date" desc
                LIMIT(500)
            '''

unfinished_data = '''select *
                from scraper_meta
                where "IsDataFinished" = false
                and "County" = '{county}'
                and "Doc_Count" > 0
                order by "FC_Recorded_Date" desc
                LIMIT(300)
            '''
            
unscrpaped_query =  """
        with full_dates as (
        select * from generate_series('2019-01-01', '2021-08-30', interval '1 day') AS dates
        left join lateral 
        (select "County", "Transaction_Type" from scraper_meta
        group by "County", "Transaction_Type")  as sm 
        on true
        order by "County", "Transaction_Type", dates 
        ),
        missing_dates as (
        select fd."County", fd."Transaction_Type", DATE(fd.dates) as "FC_Recorded_Date" from full_dates fd
        left join scraper_meta sm on sm."County" = fd."County" and sm."Transaction_Type" = fd."Transaction_Type" and sm."FC_Recorded_Date" = fd.dates
        where sm."County" is null
        order by "County", "Transaction_Type", dates
        ),
        unscraped as (
        select * from missing_dates
        cross join (VALUES ('data'), ('pdf')) sc("scrape_type")
        union
        select "County", "Transaction_Type", "FC_Recorded_Date", 'data' as "scrape_type"
        from scraper_meta
        where "IsDataFinished" = 'false' and "Doc_Count" != 0
        union
        select "County", "Transaction_Type", "FC_Recorded_Date", 'pdf' as "scrape_type"
        from scraper_meta
        where "IsPdfFinished" = 'false' and "Doc_Count" != 0
        )
        select * from unscraped
        where "County" = '{county}'
        order by "County", "FC_Recorded_Date", "Transaction_Type"
            
    """