import pandas as pd
import numpy as np
from datetime import timedelta, date

from common.sqlalchemy_engine import get_engine, start_engine
from common.connect_db import ConnectDB

import queries

env = 'postgresql'
connect_db = ConnectDB(env)

def find_date_limit():
    """
    Set limit on remedy scraping to be sure not to interfere with current day scraping

    find the latest completed scrape run and substract a day
    """
    session = start_engine(env)
    results = session.execute(queries.get_date_limit.format(county=county)).fetchone()
    session.close()
    return (results[0] - timedelta(days=1)).strftime('%Y-%m-%d')

def get_transaction_types(transaction_types):
    df = pd.read_sql_query(
            f'''select distinct "Transaction_Type" 
                from scraper_meta
                where "Transaction_Type" in ('{transaction_types}')
                and "County" = '{county}' 
            ''',
            connect_db.connection()
            )
    return df['Transaction_Type'].tolist()

def get_transaction_types_all():
    df = pd.read_sql_query(
            f'''select distinct "Transaction_Type" 
                from scraper_meta
                where "County" = '{county}' 
            ''',
            connect_db.connection()
            )
    return df['Transaction_Type'].tolist()

def get_existing_records(transaction_types):
    df = pd.read_sql_query(
            f'''select * from scraper_meta
            where "County" = '{county}'
            and "FC_Recorded_Date" >= '{start_date}'
            and (("Doc_Count" > 0 and "IsDataFinished" = true)
                or "Doc_Count" = 0
                or ("Doc_Count" is null and "IsDataFinished" = true) )
            and "Transaction_Type" in ('{transaction_types}')
            order by "FC_Recorded_Date" asc
            ''',
            connect_db.connection()
            )
    df['FC_Recorded_Date'] = df['FC_Recorded_Date'].astype(str)
    return df[['County','FC_Recorded_Date','Transaction_Type']]

def get_existing_records_all():
    df = pd.read_sql_query(
            f'''select * from scraper_meta
            where "County" = '{county}'
            and "FC_Recorded_Date" >= '{start_date}'
            and (("Doc_Count" > 0 and "IsDataFinished" = true)
                or "Doc_Count" = 0
                or ("Doc_Count" is null and "IsDataFinished" = true) )
            order by "FC_Recorded_Date" asc
            ''',
            connect_db.connection()
            )
    df['FC_Recorded_Date'] = df['FC_Recorded_Date'].astype(str)
    return df[['County','FC_Recorded_Date','Transaction_Type']]


def cartesian_product(data):
    index = pd.MultiIndex.from_product(data.values(), names=data.keys())
    return pd.DataFrame(index=index).reset_index()

def main():
    if transaction_types:
        types = '\',\''.join(x for x in transaction_types)
        transaction_type = get_transaction_types(types)
        existing_records = get_existing_records(types)
    else:
        transaction_type = get_transaction_types_all()
        existing_records = get_existing_records_all()

    df_2 = pd.date_range(start=start_date, end=end_date)
    cal_df = cartesian_product({'Transaction_Type': transaction_type,
                    'FC_Recorded_Date': df_2,
                    'Scrape_Type': ['data','pdf']})
    cal_df['County'] = county
    cal_df['FC_Recorded_Date'] = cal_df['FC_Recorded_Date'].astype(str)

    common = cal_df.merge(existing_records, on=['County','FC_Recorded_Date','Transaction_Type'])
    missing = pd.concat([cal_df,common]).drop_duplicates(keep=False)
    missing = missing.head(400)
    print(missing.head(20))
    missing.to_csv(f"data/{county}_remedy.csv", index=False)

if __name__ == "__main__":
    env = 'postgresql'
    county = 'Palm-Beach'
    #transaction_types = ['ASM','NS','SAT','DEED','MTG']
    transaction_types = ['MTG','ASG']
    start_date = '2019-08-01'
    end_date = find_date_limit()
    connect_db = ConnectDB(env)
    main()