import pandas as pd
from datetime import timedelta, date

from common.sqlalchemy_engine import get_engine, start_engine
from common.connect_db import ConnectDB

import queries

def find_date_limit():
    """
    Set limit on remedy scraping to be sure not to interfere with current day scraping

    find the latest completed scrape run and substract a day
    """
    session = start_engine(env)
    results = session.execute(queries.get_date_limit.format(county=county)).fetchone()
    session.close()
    return (results[0] - timedelta(days=1)).strftime('%Y-%m-%d')

def find_records():
    """
    execute the query from queries.py to determine which county, date, and transaction type
    needs to be scraped 
    """   
    df = pd.read_sql_query(
        queries.missing_pdfs.format(
            county=county, 
            date_limit=find_date_limit()
            ), 
        connect_db.connection()
        )
    
    df['County'] = df['County'].apply(lambda x: x.lower())
    df['Scrape_Type'] = 'pdf'
    return df[['County','FC_Recorded_Date','Transaction_Type','Scrape_Type']]

def main():
    """
    df dumped to scraper_remedy.csv will be used to generate dag for remedy-scraper-pipeline
    
    git commit the csv to master to change current dag
    
    WARNING: dags are mutable so commiting the csv in the middle of run will break current run
    """
    df = find_records()
    print(df.head(20))
    df.to_csv("data/scraper_remedy.csv", index=False)


if __name__ == "__main__":
    env = 'postgresql'
    county = 'Travis'
    connect_db = ConnectDB(env)
    main()