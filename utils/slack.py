from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook

def message(status, conn_id, dag):
    webhook_token = BaseHook.get_connection(conn_id).password

    if status == 'start':
        msg = f""" {dag.dag_id} Started """

    alert = SlackWebhookOperator(
        task_id='slack_message',
        http_conn_id=conn_id,
        webhook_token=webhook_token,
        message=msg,
        link_names=True,
        username='airflow',
    )

    return alert 