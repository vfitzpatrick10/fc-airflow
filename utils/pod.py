import os
from airflow.contrib.kubernetes.secret import Secret

def get_postgres_secret(host: str, username: str, password: str, dbname: str, secret: str) -> list:
    return [
        Secret('env', host, secret, 'host'),
        Secret('env', username, secret, 'username'),
        Secret('env', password, secret, 'password'),
        Secret('env', dbname, secret, 'dbname')
    ]

def get_s3_secret(access_key_id: str, secret_access_key: str, region: str, bucket: str) -> list:
    return [
        Secret('env', access_key_id, 's3-bucket', 'access_key_id'),
        Secret('env', secret_access_key, 's3-bucket', 'secret_access_key'),
        Secret('env', region, 's3-bucket', 'region'),
        Secret('env', bucket, 's3-bucket', 'bucket')
    ]

def small_resources():
    return {
        "request_memory": "500Mi",
        "limit_memory": "1500Mi",
        "request_cpu": "500m",
        "limit_cpu": "1000m"
    }

def medium_resources():
    return {
        "request_memory": "2Gi",
        "limit_memory": "5Gi",
        "request_cpu": "1000m",
        "limit_cpu": "1500m"
    }

def large_resources():
    return {
        "request_memory": "5Gi",
        "limit_memory": "8Gi",
        "request_cpu": "2000m",
        "limit_cpu": "2500m"
    }

def xlarge_resources():
    return {
        "request_memory": "10Gi",
        "limit_memory": "13Gi",
        "request_cpu": "2500m",
        "limit_cpu": "3000m"
    }